<?php
    session_start();
    if(!isset($_SESSION['login'])) {
        header('LOCATION:index.php'); die();
    }


if (is_writeable('../includes/website_setting.conf')){
   $write=true;
} else {
   $write=false;
}

$data_file = unserialize(base64_decode(file_get_contents('../includes/website_setting.conf')));
extract($data_file);

//echo '<pre>';
//print_r($data_file);
//echo '</pre>';

$config_url = 'admin.php';

function _c($value, $other = '')
{
if (isset($value) and $value != '') {
return $value;
}
else {
return $other;
}
}
  
?>
<?php include '../header.php';?>

<form class="form-horizontal" method="post" action="<?php if ($write==false){ echo '#'; } else {echo $config_url;} ?>">
  <div class="bd-example bd-example-tabs">
    <ul class="nav nav-tabs" id="myTab" role="tablist">
      <li class="nav-item">
        <a class="nav-link active show" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Keyword</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Ads</a>
      </li>
    </ul>
    <div class="tab-content" id="myTabContent">
      <div class="tab-pane fade active show" id="home" role="tabpanel" aria-labelledby="home-tab">

        <div class="form-group">
          <label class="col-md-4 control-label" for="sitename">
                  <b>Niche Keyword</b>
                </label>
          <div class="col-md-4">
            <input id="title" name="niche" value="<?php echo _c($niche);?>" placeholder="WebMusic" class="form-control input-md" required="" type="text">
            <span class="help-block">
                    Niche Keyword Here
                  </span>
          </div>
        </div>

        <div class="form-group">
          <label class="col-md-4 control-label" for="sitename">
                  <b>Website Name</b>
                </label>
          <div class="col-md-4">
            <input id="sitename" name="sitename" value="<?php echo _c($sitename);?>" placeholder="WebMusic" class="form-control input-md" required="" type="text">
            <span class="help-block">
                    Add Your Sitename Which use almost in Whole Website!
                  </span>
          </div>
        </div>

        <div class="form-group">
          <label class="col-md-4 control-label" for="sitename">
                  <b>Keyword Home</b>
                </label>
          <div class="col-md-4">
            <input id="keyword_home" name="keyword_home" value="<?php echo _c($keyword_home);?>" placeholder="WebMusic" class="form-control input-md" required="" type="text">
            <span class="help-block">
                    Keyword home here
                  </span>
          </div>
        </div>

        <div class="form-group">
          <label class="col-md-4 control-label" for="sitename">
                  <b>Website Description Home</b>
                </label>
          <div class="col-md-4">
            <input id="description" name="description" value="<?php echo _c($description);?>" placeholder="description" class="form-control input-md" required="" type="text">
            <span class="help-block">
                    Description Here
                  </span>
          </div>
        </div>

        <div class="form-group">
          <label class="col-md-4 control-label" for="sitename">
                  <b>Website Title</b>
                </label>
          <div class="col-md-4">
            <input id="title" name="title" value="<?php echo _c($title);?>" placeholder="title" class="form-control input-md" required="" type="text">
            <span class="help-block">
                    Title Here
                  </span>
          </div>
        </div>

        <div class="form-group">
          <label class="col-md-4 control-label" for="sitename">
                  <b>Speparator</b>
                </label>
          <div class="col-md-4">
            <input id="title" name="separator" value="<?php echo _c($separator);?>" placeholder="Speparator" class="form-control input-md" required="" type="text">
            <span class="help-block">
                    Speparator Here
                  </span>
          </div>
        </div>
      </div>
      <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
        <div class="form-group">
          <div class="col-md-4">
            <textarea class="form-control" id="kw_list" name="kw_list" rows="10" placeholder="KW HERE"><?php echo _c($kw_list);?></textarea>
            <span class="help-block">
                    Keyword
                  </span>
          </div>
        </div>


      </div>
      <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">

        <div class="form-group">
          <label class="col-md-4 control-label" for="ads_validator">
                  <b>Ads Validator</b>
                </label>
          <div class="col-md-4">
            <input id="ads_validator" name="ads_validator" value="<?php echo _c($ads_validator);?>" placeholder="Yes/No" class="form-control input-md" required="" type="text">
            <span class="help-block">
                    Ads Validator yes or no
                  </span>
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-md-4 control-label" for="ads_filter">
                  <b>Ads Filter</b>
                </label>
          <div class="col-md-4">
            <input id="ads_filter" name="ads_filter" value="<?php echo _c($ads_filter);?>" placeholder="Badwords Url" class="form-control input-md" required="" type="text">
            <span class="help-block">
                    Ads Filter Badwords
                  </span>
          </div>
        </div>
        
        <div class="form-group">
          <div class="col-md-6">
            <textarea class="form-control" id="ads1" name="ads1" rows="10" placeholder="Ads1"><?php echo _c($ads1);?></textarea>
            <span class="help-block">
                    Ads 1
                  </span>
          </div>
        </div>

        <div class="form-group">

          <div class="col-md-6">
            <textarea class="form-control" id="ads2" name="ads2" rows="10" placeholder="Ads2"><?php echo _c($ads2);?></textarea>
            <span class="help-block">
                    Ads 2
                  </span>
          </div>
        </div>

        <div class="form-group">

          <div class="col-md-6">
            <textarea class="form-control" id="ads3" name="ads3" rows="10" placeholder="Ads3"><?php echo _c($ads3);?></textarea>
            <span class="help-block">
                    Ads 3
                  </span>
          </div>
        </div>

        <div class="form-group">

          <div class="col-md-6">
            <textarea class="form-control" id="ads4" name="ads4" rows="10" placeholder="Ads4"><?php echo _c($ads4);?></textarea>
            <span class="help-block">
                    Ads 4
                  </span>
          </div>
        </div>
        
        <div class="form-group">

          <div class="col-md-6">
            <textarea class="form-control" id="ads_buruk" name="ads_buruk" rows="10" placeholder="Ads4"><?php echo _c($ads_buruk);?></textarea>
            <span class="help-block">
                    Ads Buruk
                  </span>
          </div>
        </div>
        
        
      </div>
    </div>
  </div>



  <div class="form-group">
    <label class="col-md-4 control-label" for="submit">
                </label>
    <div class="col-md-4">
      <input type="submit" id="submit" name="submit" value="Save Settings" class="btn btn-primary">
    </div>
  </div>
</form>
<?php

include '../footer.php';
//$_POST['yt_cache'] = 'enable';
/*
if (isset($_POST['yt_cache'])) {
$_POST['yt_cache'] = 'enable';
}
else {
$_POST['yt_cache'] = 'disable';
}
*/
if ($_POST) {
$save = base64_encode(serialize($_POST));
file_put_contents('../includes/website_setting.conf', $save);
$data = unserialize(base64_decode(file_get_contents('../includes/website_setting.conf')));

extract($data);
  
  //echo '<h1>Setting updated succesfully, Redirecting to Configuration Page</h1>';
echo '<meta http-equiv="refresh" content="0;url=' . $config_url . '" />';
                  die();
                  
}



?>