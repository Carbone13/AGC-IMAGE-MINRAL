<?php 
require_once 'function/function.php';

$data_file = unserialize(base64_decode(file_get_contents('includes/website_setting.conf')));
extract($data_file);

$keyword_home = explode(',',$niche);

  
$seed = floor(time()/(60*5));
srand($seed);

shuffle($keyword_home);
//print_r ($keyword_home);

$bing_img = getdata('http://api.mp3kas.co.uk/eco-images/'.$keyword_home[0]);
$bing_result_title_data = getdata('http://api.mp3kas.co.uk/eco-result/'.$keyword_home[0]);
$bing_result_desc = getdata('http://api.mp3kas.co.uk/eco-result_desc/'.$keyword_home[0]);

//print_r($bing_result_titl_data);

foreach($bing_result_title_data as $bing_result_result){
  $bing_result_title[] = $bing_result_result;
  $bing_result_url[] = strtolower(str_replace(' ','-',(cleaner($bing_result_result))));
}

//print_r($bing_result_title);
foreach($bing_result_desc as $bing_result_result_desc){
  $bing_result_desc[] = $bing_result_result_desc;
}



foreach($bing_img as $bing_img_result){
  $bing_images[] = str_replace(array('https://','http://'),'https://i0.wp.com/',$bing_img_result);
  //$bing_images_name[] = str_replace(array('.com'),'',($bing_img_result->nama));
  //$bing_images_url[] = slug_url($bing_img_result->nama);
}
//print_r($bing_images_name);

//echo image_to_title($bing_images[0]);

$count_result_image = count($bing_img);
$count_result_desc = count($bing_result_title_data);

if($count_result_image > $count_result_desc){
  //$rand_count = rand(1,6);
  $count_img = $count_result_desc;
}else{
  $count_img = $count_result_image;
}

function _c($value, $other = '')
{
if (isset($value) and $value != '') {
return $value;
}
else {
return $other;
}
}
?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?php echo _c($description);?>">

    <title><?php echo _c($sitename);?> <?php echo _c($separator);?> <?php echo _c($title);?></title>

   <?php include 'header.php';?>

    <!-- Page Content -->
    <div class="container">

      <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-8">
          <div class="my-4">
            <h1><?php echo _c($sitename);?> <?php echo _c($title);?>
            </h1>
            <small><?php echo _c($description);?></small>
          </div>
          <!-- Blog Post -->
          <?php for($i=0;$i<$count_img;$i++){?>
          <div class="card mb-4">
            <a href="<?php echo url_route_media(image_to_title($bing_images[$i]));?>" alt="<?php echo basename($bing_images[$i]);?>"><img onerror="setErrorImage_head(this)" class="card-img-top" src="https://i0.wp.com/i.redd.it/ounq1mw5kdxy.gif" data-src="<?php echo $bing_images[$i];?>?resize=700,300" alt="<?php echo basename($bing_images[$i])?>"></a>
            <div class="card-body">
              <a href="<?php echo url_route($bing_result_url[$i]);?>"><h2 class="card-title"><?php echo ucwords(cleaner($bing_result_title[$i]));?></h2></a>
              <p class="card-text"><?php echo $bing_result_desc[$i];?></p>
              <a href="<?php echo url_route($bing_result_url[$i]);?>" class="btn btn-primary">Read More &rarr;</a>
            </div>
            <div class="card-footer text-muted">
              Posted on <?php echo date('F d, Y', strtotime( '-'.strlen($bing_result_desc[$i]).' days'));?> by
              <a href="/author/<?php $namerand = rand(0,152); echo strtolower($fakename[$namerand]);?>" rel="nofollow"><?php echo $fakename[$namerand];?></a>
            </div>
          </div>
          <?php } ?>
          <!-- End Blog Post -->

          <!-- Pagination Disabled By React Protection
          <ul class="pagination justify-content-center mb-4">
            <li class="page-item">
              <a class="page-link" href="#">&larr; Older</a>
            </li>
            <li class="page-item disabled">
              <a class="page-link" href="#">Newer &rarr;</a>
            </li>
          </ul>
          -->

        </div>

        <?php include 'sidebar.php';?>

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->
    
    <?php include 'footer.php';?>