<?php 
require_once 'function/function.php';

if (isset($_GET['q'])) {
  $keyword_search = $_GET['q'];
}
   
$data_file = unserialize(base64_decode(file_get_contents('includes/website_setting.conf')));
extract($data_file);

$keyword_search = urlencode($keyword_search);
$bing_img = getdata('http://nodejs-serv3157194.codeanyapp.com:7878/index?id='.$keyword_search.'&num=20');
$bing_result = getdata('http://nodejs-serv3157194.codeanyapp.com:7777/index?id='.$keyword_search.'&num=20');

foreach($bing_result as $bing_result_result){
  $bing_result_title[] = $bing_result_result->title;
  $bing_result_url[] = strtolower(str_replace(' ','-',(cleaner($bing_result_result->title))));
  $bing_result_desc[] = $bing_result_result->detail;
}

foreach($bing_img as $bing_img_result){
  $bing_images[] = str_replace(array('https://','http://'),'https://i0.wp.com/',($bing_img_result->url));
  $bing_images_name[] = str_replace(array('.com'),'',($bing_img_result->nama));
  $bing_images_url[] = slug_url($bing_img_result->nama);
}
//print_r($bing_images_name);

$count_result_image = count($bing_img);
$count_result_desc = count($bing_result);

if($count_result_image > $count_result_desc){
  //$rand_count = rand(1,6);
  $count_img = $count_result_desc;
}else{
  $count_img = $count_result_image;
}

function _c($value, $other = '')
{
if (isset($value) and $value != '') {
return $value;
}
else {
return $other;
}
}
?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Search Result For <?php if (isset($_GET['q'])) echo ucwords($_GET['q']); ?> , <?php echo _c($description);?>">

    <title>Your Search For "<?php if (isset($_GET['q'])) echo ucwords($_GET['q']); ?>" - <?php echo _c($sitename);?> <?php echo _c($separator);?> <?php echo _c($title);?></title>

   <?php include 'header.php';?>

    <!-- Page Content -->
    <div class="container">

      <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-8">
          <div class="my-4">
            <h1>Results For : <?php if (isset($_GET['q'])) echo ucwords($_GET['q']); ?>
            </h1>
            <?php if(!empty($bing_result)){
            echo '<small>We Only Find This For You</small>';
            }else{
              echo '<small>Sorry we cannot find your request.</small>';
            }
            ?>
          </div>
          <!-- Blog Post -->
          <?php for($i=0;$i<$count_img;$i++){?>
          <div class="card mb-4">
            <a href="<?php echo url_route_media($bing_images_url[$i]);?>" alt="<?php echo $bing_images_name[$i];?>"><img onerror="setErrorImage_head(this)" class="card-img-top" src="https://i0.wp.com/i.redd.it/ounq1mw5kdxy.gif" data-src="<?php echo $bing_images[$i];?>?resize=700,300" alt="<?php echo $bing_images_name[$i];?>"></a>
            <div class="card-body">
              <a href="<?php echo url_route($bing_result_url[$i]);?>"><h2 class="card-title"><?php echo ucwords(str_replace('-',' ',(cleaner($bing_result_title[$i]))));?></h2></a>
              <p class="card-text"><?php echo $bing_result_desc[$i];?></p>
              <a href="<?php echo url_route($bing_result_url[$i]);?>" class="btn btn-primary">Read More &rarr;</a>
            </div>
            <div class="card-footer text-muted">
              Posted on <?php echo date('F d, Y', strtotime( '-'.strlen($bing_result_desc[$i]).' days'));?> by
              <a href="/author/<?php $namerand = rand(0,152); echo strtolower($fakename[$namerand]);?>" rel="nofollow"><?php echo $fakename[$namerand];?></a>
            </div>
          </div>
          <?php } ?>
          <!-- End Blog Post -->

          <!-- Pagination Disabled By React Protection
          <ul class="pagination justify-content-center mb-4">
            <li class="page-item">
              <a class="page-link" href="#">&larr; Older</a>
            </li>
            <li class="page-item disabled">
              <a class="page-link" href="#">Newer &rarr;</a>
            </li>
          </ul>
          -->

        </div>

        <?php include 'sidebar.php';?>

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->
    
    <?php include 'footer.php';?>