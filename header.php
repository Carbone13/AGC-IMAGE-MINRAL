 <!-- Bootstrap core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/blog-home.css?ver=1.2ffff" rel="stylesheet">

<style>
nav.navbar {
  background: green;
}
.navbar a.navbar-brand {
  color: white;
}
.navbar a.navbar-brand:hover {
  color: green;
}
.navbar ul.navbar-nav li.nav-item a.nav-link {
  color: white;
}
.navbar-light span.navbar-toggler-icon {
  color: white;
}
button.navbar-toggler.navbar-toggler-right {
  border-color: yellow;
}
@media (max-width: 768px) {
  nav.navbar {
    background: lightgray;
  }
}
</style>
  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top">
    <!--<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">-->
      <div class="container">
        <a class="navbar-brand" href="/"><?php echo ucfirst($host);?></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="/">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/about-us">About</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/dmca">DMCA</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/contact-us">Contact</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>